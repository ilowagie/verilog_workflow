# Ilowagie's Verilog Workflow

This project is to test out and show others a verilog workflow using only open-source tools.
The Verilog project is a wishbone slave that provides Uart serial communication.

The goal is to first get a workflow that includes simple unit tests that drive stimuli and checks outputs for each module separately.
This is can be handy to test if everything compiles and if the written verilog does the basic things that you expect it to do.
Passing these tests is no guarantee that the verilog is completely correct however.

Once the unit tests that check all the expected inputs/outputs all pass, it is time to use formal verification to check the actual correctness of the design.
Formal verification can find errors that you didn't even thought about when writing the unit tests.


## Tools Used

- **meson** build system
- **verilator** Verilog simulator
- **criterion** unit test framework
- **Yosys** for synthesizing Verilog (and dependency for SymbiYosys)
- **SymbiyYosys** for formal verification
- **Yices** SMT-solver used by SymbiYosys

## Tools that -- most likely -- will be used in the future\

- **nextPNR** for FPGA placement, routing, and bitstream generation

## Notes about some of the tools

### Meson build system

For a while now, I prefer meson with the ninja generator and clang compiler for building my projects.
However, it seems that the dominant build system in the c++ world is cmake with make as a generator and gcc as compiler.
This is the setup that verilator assumes and this is the setup in which verilator just works.
In cmake, just look for the `verilator` package and call the `verilate` function and the project builds itself.

But, as a test (or maybe some kind of masochistic impulse), I chose to build the project with meson.
This forced me to look into what files verilator generates, how you're supposed to use them, and what assumptions verilator makes.
After a lot of documentation reading and trial-and-error, I finally managed to write the needed `meson.build` files.
The meson files can work with the chosen directory structure where verilog HDL and c++ tests are separated in their own directories.
