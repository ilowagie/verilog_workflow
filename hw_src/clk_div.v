`default_nettype    none

module clk_div
    #(
        CLK_DIV = 1
    )
    (
        input   wire rst,
        input   wire clk,
        output  wire stb
    );

    reg [31:0]  clk_counter;
    initial clk_counter = CLK_DIV - 1'b1;

    assign stb = (clk_counter == 0);

    // step 1 clk_division
    always @(posedge clk)
    begin
        if (rst)
            // setting same as inital values
            clk_counter <= CLK_DIV - 1'b1;
        else
        begin
            if (clk_counter == 0)
                // counter has reached end
                //  -> reset count
                clk_counter <= CLK_DIV - 1'b1;
            else
                // normal counting
                clk_counter <= clk_counter - 1'b1;
        end
    end

endmodule
