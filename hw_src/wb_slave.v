`default_nettype none

module wb_slave
    (
        // wishbone names are the same as in the standard,
        // but lowercase
        input   wire        rst_i,
        input   wire        clk, // needs to be just clk to work with ClockedTestBench template
        // there are 3 valid addresses:
        //  1 -> read how much place there's left in the tx_buffer
        //  2 -> read if rx_data is valid
        //  3 -> read data from rx_buffer
        // when requesting 0, you get all 0
        input   reg[1:0]    adr_i,
        input   reg[7:0]    dat_i,
        input   reg         sel_i,
        input   reg         we_i,
        input   reg         stb_i,
        input   reg         cyc_i,

        output  reg[7:0]    dat_o,
        output  wire        ack_o,
        output  reg         err_o,

        // non-wishbone signals
        input   wire[7:0]   tx_buf_space,
        input   wire        rx_valid,
        input   wire[7:0]   rx_data,

        output  reg         tx_req,
        output  reg[7:0]    tx_data
    );

    initial dat_o   = 8'h00;
    initial err_o   = 1'b0;
    initial tx_req  = 1'b0;
    initial tx_data = 8'h00;

    localparam WB_IDLE    = 3'h0,
               WB_R_WAIT  = 3'h1,
               WB_R_START = 3'h2,
               WB_W_WAIT  = 3'h3,
               WB_W_START = 3'h4;

    reg [2:0] state;
    initial state = WB_IDLE;

    `ifdef FORMAL
        formal_wbslave #(
            .ADDR_WIDTH (2),
            .DATA_WIDTH (8),
            .SEL_WIDTH  (1)
        ) formal_description (
            .rst_i (rst_i), .clk (clk),
            .adr_i (adr_i), .dat_i (dat_i), .sel_i (sel_i), .we_i (we_i), .stb_i (stb_i), .cyc_i (cyc_i),
            .dat_o (dat_o), .ack_o (ack_o), .err_o (err_o)
        );
    `endif

    // Timeout generation
    // ==================

    localparam TIMEOUT = 16'h0019;

    wire internal_rst;

    reg  timeout_reached;
    initial timeout_reached = 1'b0;

    reg[15:0]  timeout_timer;
    initial timeout_timer = 16'h0000;

    assign internal_rst = timeout_reached || rst_i;

    always @(posedge clk)
    begin
        if (internal_rst)
        begin
            err_o           <= 1'b0;
            timeout_reached <= 1'b0;
            timeout_timer   <= 16'h0000;
        end
        else
        begin
            if (state == WB_W_WAIT || state == WB_R_WAIT)
            begin
                if (timeout_timer >= TIMEOUT - 1)
                begin
                    err_o           <= 1'b1;
                    timeout_reached <= 1'b1;
                    timeout_timer   <= 16'h0000;
                end
                else
                begin
                    err_o           <= 1'b0;
                    timeout_reached <= 1'b0;
                    timeout_timer   <= timeout_timer + 16'h0001;
                end
            end
            else
            begin
                err_o           <= 1'b0;
                timeout_reached <= 1'b0;
                timeout_timer   <= 16'h0000;
            end
        end
    end


    // Main operation
    // ==============

    reg ack_candidate;
    initial ack_candidate = 1'b0;
    assign ack_o = !err_o && ack_candidate;

    always @(posedge clk)
    begin
        if (internal_rst)
        begin
            state   <= WB_IDLE;
            dat_o   <= 8'h00;
            ack_candidate   <= 1'b0;
            tx_req  <= 1'b0;
            tx_data <= 8'h00;
        end
        else
        begin
            case (state)
                WB_IDLE:
                begin
                    if (stb_i & cyc_i)
                    begin
                        if (we_i)
                        begin
                            // write request
                            if (tx_buf_space == 8'h00)
                            begin
                                state   <= WB_W_WAIT;

                                dat_o   <= 8'h00;
                                ack_candidate   <= 1'b0;
                                tx_req  <= 1'b0;
                                tx_data <= 8'h00;
                            end
                            else
                            begin
                                state   <= WB_W_START;

                                dat_o   <= 8'h00;
                                ack_candidate   <= 1'b1;
                                tx_req  <= 1'b1;
                                tx_data <= (sel_i) ? dat_i : 8'h00;
                            end
                        end
                        else
                        begin
                            // read request
                            if ((adr_i == 2'b11) && !(rx_valid))
                            begin
                                state   <= WB_R_WAIT;

                                dat_o   <= 8'h00;
                                ack_candidate   <= 1'b0;
                                tx_req  <= 1'b0;
                                tx_data <= 8'h00;
                            end
                            else
                            begin
                                state   <= WB_R_START;

                                ack_candidate   <= 1'b1;
                                tx_req  <= 1'b0;
                                tx_data <= 8'b00;

                                case (adr_i)
                                    2'b01:
                                        dat_o   <= tx_buf_space;
                                    2'b10:
                                        dat_o   <= (rx_valid) ? 8'h01 : 8'h00;
                                    2'b11:
                                        dat_o   <= rx_data;
                                    default:
                                        dat_o   <= 8'h00;
                                endcase
                            end
                        end
                    end
                end
                WB_W_WAIT:
                begin
                    if (tx_buf_space == 8'h00)
                        state   <= WB_W_WAIT;
                    else
                    begin
                        state   <= WB_W_START;

                        dat_o   <= 8'h00;
                        ack_candidate   <= 1'b1;
                        tx_req  <= 1'b1;
                        tx_data <= (sel_i) ? dat_i : 8'h00;
                    end
                end
                WB_R_WAIT:
                begin
                    if ((adr_i == 2'b11) && !(rx_valid))
                        state   <= WB_R_WAIT;
                    else
                    begin
                        state   <= WB_R_START;

                        ack_candidate   <= 1'b1;
                        tx_req  <= 1'b0;
                        tx_data <= 8'b00;

                        case (adr_i)
                            2'b01:
                                dat_o   <= tx_buf_space;
                            2'b10:
                                dat_o   <= (rx_valid) ? 8'h01 : 8'h00;
                            2'b11:
                                dat_o   <= rx_data;
                            default:
                                dat_o   <= 8'h00;
                        endcase
                    end
                end
                // single read/write only
                //  `-> 1 cycle after handling requests, go to idle again
                WB_R_START:
                begin
                    state <= WB_IDLE;
                    dat_o   <= 8'h00;
                    ack_candidate   <= 1'b0;
                    tx_req  <= 1'b0;
                    tx_data <= 8'h00;
                end
                WB_W_START:
                begin
                    state <= WB_IDLE;
                    dat_o   <= 8'h00;
                    ack_candidate   <= 1'b0;
                    tx_req  <= 1'b0;
                    tx_data <= 8'h00;
                end
                default:
                begin
                    state <= WB_IDLE;
                    dat_o   <= 8'h00;
                    ack_candidate   <= 1'b0;
                    tx_req  <= 1'b0;
                    tx_data <= 8'h00;
                end
            endcase
        end
    end

endmodule
