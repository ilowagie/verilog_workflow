verilog_src += ['uart_tx.v', 'wb_slave.v', 'uart_buffer.v']

verilator = find_program('verilator')
make = find_program('make') # verilator generates make files, so no ninja

# the destination for all verilator-generated files is the build root
#   this is because there was no other way for the custom make target to find the generated make files
#   As far as I've tried, custom_target does not allow you to set the input to a path rather than a file
verilator_destination = meson.current_build_dir()
verilator_source_dir = meson.current_source_dir()
# add it as an include dir, since this is where the generated headers go
# this way we don't have to specify a hard-coded build directory in our #include statements
add_global_arguments('-I' + verilator_destination, language : 'cpp')
extra_verilator_args += ['-I' + verilator_source_dir]

verilator_elems = []
# first generate the .mk, .cpp and .h files from all .v files
foreach i : verilog_src
  basename = i.split('.')[0] # remove .v
  verilator_base = 'V' + basename
  mk_name = verilator_base + '.mk'
  classes_mk = verilator_base + '_classes.mk'
  tgt = custom_target(mk_name,
                      output : mk_name,
                      input : i,
                      command : [verilator, '--Mdir', verilator_destination, '--cc', '--trace', '@INPUT@'] + extra_verilator_args)
  tgt_classes = custom_target(classes_mk,
                              output : classes_mk,
                              input : i,
                              command : [verilator, '--Mdir', verilator_destination, '--cc', '--trace', '@INPUT@'] + extra_verilator_args)
  elem = [mk_name, tgt, tgt_classes]
  verilator_elems += [elem]
endforeach
# then build .cpp file using .mk files with make
foreach i : verilator_elems
  in_name = i[0]
  dep = [i[1], i[2]]
  basename = in_name.split('.')[0] # remove .mk
  a_name = basename + '__ALL.a'
  tgt = custom_target(a_name,
                      output : a_name,
                      input : dep[0],
                      command : [make, '-C', verilator_destination, '-f', '@PLAINNAME@'],
                      depends : dep)
  special_targets += [tgt]
endforeach

