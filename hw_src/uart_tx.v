`default_nettype    none

module uart_tx
    #(
        parameter CLK_DIV   = 4
    )
    (
        input   wire        clk,
        input   wire        rst,
        input   wire [7:0]  parallel_data_in,
        input   reg         req,
        output  reg         busy,
        output  wire        serial_uart_out
    );

    initial busy    = 1'b0;
    initial serial_uart_out = 1'b1;

    // localparam for state is annoying, this is where VHDL is superior
    localparam S_START = 4'b0000,
               //S_B0    = 4'b0001,
               //S_B1    = 4'b0010,
               //S_B2    = 4'b0011,
               //S_B3    = 4'b0100,
               //S_B4    = 4'b0101,
               //S_B5    = 4'b0110,
               //S_B6    = 4'b0111,
               S_B7    = 4'b1000,
               S_IDLE  = 4'b1010;

    // reg [$clog2(CLK_DIV):0] counter; // does this work?
    reg [7:0]   shift_reg;
    reg [3:0]   shift_state;
    initial shift_reg   = 8'hff; // UART is active low => high is idle
    initial shift_state = S_IDLE;

    // PART 1: UART strobe generation

    wire stb;
    clk_div #(CLK_DIV) clock_divider (rst, clk, stb);

    // PART 2: MEALY FSM

    always @(posedge clk)
    begin
        if (rst)
        begin
            shift_state <= S_IDLE;
        end
        else if (stb)
        begin
            if ((req) && (shift_state == S_IDLE))
                shift_state <= S_START;
            else if (shift_state != S_IDLE)
                if (shift_state == S_B7)
                    shift_state <= S_IDLE;
                else
                    shift_state <= shift_state + 1'b1;
        end
    end

    // careful: don't want to make it a "more than Moore" state machine

    always @(posedge clk)
    begin
        if (rst)
        begin
            busy            <= 1'b0;
            serial_uart_out <= 1'b1;
            shift_reg       <= 8'hff;
        end
        else if (stb)
        begin
            if ((req) && (shift_state == S_IDLE))
            begin
                busy            <= 1'b1;
                serial_uart_out <= 1'b0; // send start bit
                shift_reg       <= parallel_data_in; // sample data
            end
            else if (shift_state != S_IDLE)
            begin
                if (shift_state == S_B7)
                begin
                    busy            <= 1'b0;
                    serial_uart_out <= 1'b1; // stop bit
                    shift_reg       <= 8'hff;
                end
                else
                begin
                    busy                         <= 1'b1;
                    {serial_uart_out, shift_reg} <= {shift_reg, 1'b1};
                end
            end
        end
    end

endmodule
