`default_nettype none

module uart_buffer
#(
    parameter SPACE_LOG2    = 3,
    parameter MAX_ADR       = 6,
    parameter WIDTH         = 8
)
(
    input   wire                    rst,
    input   wire                    clk,
    input   reg[WIDTH-1:0]          data_in,
    input   wire                    read_en,
    input   wire                    write_en,

    output  wire                    data_valid,
    output  reg[WIDTH-1:0]          data_out,
    output  wire[SPACE_LOG2-1:0]    space_left
);

always assert (1 >> SPACE_LOG2 == MAX_ADR+2);

initial data_out = {WIDTH{1'b0}};
initial space_left = {SPACE_LOG2{1'b1}};

reg intern_valid;
initial intern_valid = 1'b0;
assign data_valid = intern_valid;

// memory will behave like circular buffer
// new data is thrown away when buffer is full
// read data is thrown away after reading
reg[SPACE_LOG2-1:0] cur_write_mem;
initial cur_write_mem = {SPACE_LOG2{1'b0}};
reg[SPACE_LOG2-1:0] cur_read_mem;
initial cur_read_mem = {SPACE_LOG2{1'b0}};

reg [WIDTH-1:0] mem [MAX_ADR:0];

wire next_valid;
assign next_valid = ((|space_left) && cur_read_mem == cur_write_mem) ? 1'b0 : 1'b1;
always @(posedge clk)
    intern_valid <= (rst) ? 1'b0 : next_valid;

// reading
always @(posedge clk)
begin
    if (rst)
    begin
        data_out <= {WIDTH{1'b0}};
        cur_read_mem <= {SPACE_LOG2{1'b0}};
    end
    else
        if (read_en & next_valid)
        begin
            data_out <= mem[cur_read_mem];
            cur_read_mem <= (cur_read_mem == MAX_ADR) ? {SPACE_LOG2{1'b0}} : (cur_read_mem + 1'b1);
        end
        else
        begin
            data_out <= {WIDTH{1'b0}};
        end
end

reg full;
// is 1 if a write results in cur_read_mem == cur_write_mem
initial full = 1'b0;
// writing
always @(posedge clk)
begin
    if (rst)
    begin
        full <= 1'b0;
        cur_write_mem <= {SPACE_LOG2{1'b0}};
    end
    else
        if ((write_en) && (|space_left))
        begin
            mem[cur_write_mem] <= data_in;
            cur_write_mem <= (cur_write_mem == MAX_ADR) ? {SPACE_LOG2{1'b0}} : (cur_write_mem + 1'b1);
            if (cur_write_mem == MAX_ADR)
            begin
                cur_write_mem <= {SPACE_LOG2{1'b0}};
                if (~|cur_read_mem)
                    full <= 1'b1;
            end
            else
            begin
                cur_write_mem <= cur_write_mem + 1'b1;
                if (cur_read_mem == cur_write_mem + 1'b1)
                    full <= 1'b1;
            end
        end
        else
            if (cur_write_mem != cur_read_mem)
                full <= 1'b0;
end

// calculate available space
always @(*)
begin
    if (cur_write_mem == cur_read_mem)
        space_left = (full) ? {SPACE_LOG2{1'b0}} : {SPACE_LOG2{1'b1}};
    else
    begin
        if (cur_write_mem > cur_read_mem)
            space_left = MAX_ADR - cur_write_mem + cur_read_mem + 1;
        else
            // there's a wrap-around
            space_left = cur_read_mem - cur_write_mem;
    end
end

endmodule
