#include "uart_buffer_tb.h"

static void tb_fixture_setup (void) {
    fixture = new UartBufferTb(10);
}

static void tb_fixture_tear_down (void) {
    delete fixture;
}

void set_rst_to_0 () {
    fixture->verilated_module->rst = 0;
    fixture->verilated_module->read_en = 0;
    fixture->verilated_module->write_en = 0;
    fixture->clock_cycle();
}

void send_n_read_single_byte (uint8_t data) {
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length);
    cr_assert(!fixture->verilated_module->data_valid);
    fixture->verilated_module->data_in = data;
    fixture->verilated_module->write_en = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length - 1);
    cr_assert_eq(fixture->verilated_module->data_out, 0);

    fixture->verilated_module->write_en = 0;
    fixture->verilated_module->read_en = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length);
    cr_assert_eq(fixture->verilated_module->data_out, data);
    cr_assert(fixture->verilated_module->data_valid);
}

Test(uart_buffer, write_n_read,
     .init = tb_fixture_setup,
     .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/uart_buffer/write_n_read.vcd");

    uint8_t data_to_send = 0xb5;

    set_rst_to_0 ();
    send_n_read_single_byte (data_to_send);

    fixture->verilated_module->read_en = 0;
    fixture->clock_cycle();
    cr_assert(!fixture->verilated_module->data_valid);
}

Test(uart_buffer, use_n_rst,
     .init = tb_fixture_setup,
     .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/uart_buffer/use_n_rst.vcd");

    uint8_t data_to_send[] = {
        0xb6,
        0xb7
    };

    set_rst_to_0 ();

    send_n_read_single_byte (data_to_send[0]);

    fixture->verilated_module->data_in = data_to_send[1];
    fixture->verilated_module->write_en = 1;
    fixture->verilated_module->read_en = 0;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length - 1);

    fixture->verilated_module->rst = 1;
    fixture->clock_cycle();
    cr_assert(!fixture->verilated_module->data_valid);
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length);

    set_rst_to_0 ();
    cr_assert(!fixture->verilated_module->data_valid);
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length);

    cr_assert_eq(fixture->verilated_module->space_left, fixture->length);
    cr_assert(!fixture->verilated_module->data_valid);
    fixture->verilated_module->data_in = data_to_send[0];
    fixture->verilated_module->write_en = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length - 1);
    cr_assert_eq(fixture->verilated_module->data_out, 0);
}

Test(uart_buffer, write_n_read_to_empty,
     .init = tb_fixture_setup,
     .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/uart_buffer/write_n_read_to_empty.vcd");

    uint8_t data_to_send = 0xb8;

    set_rst_to_0 ();
    send_n_read_single_byte (data_to_send);

    fixture->verilated_module->read_en = 0;
    fixture->clock_cycle();
    cr_assert(!fixture->verilated_module->data_valid);
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length);

    fixture->verilated_module->read_en = 1;
    fixture->clock_cycle();
    cr_assert(!fixture->verilated_module->data_valid);
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length);

    send_n_read_single_byte (data_to_send);
}

Test(uart_buffer, write_to_full,
     .init = tb_fixture_setup,
     .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/uart_buffer/write_to_full.vcd");

    uint8_t data_to_send[] = {
        0xb9, 0xba, 0xbb, 0xbc,
        0xbd, 0xbe, 0xbf, // will be full here
        0xc0, 0xc1
    };

    set_rst_to_0 ();

    for (int i = 0; i < 7; i++) {
        fixture->verilated_module->write_en = 1;
        fixture->verilated_module->data_in = data_to_send[i];
        fixture->clock_cycle();
        cr_assert_eq(fixture->verilated_module->space_left, fixture->length - 1 - i);
    }

    cr_assert(fixture->verilated_module->data_valid);
    cr_assert_eq(fixture->verilated_module->space_left, 0);
    fixture->verilated_module->write_en = 1;
    fixture->verilated_module->data_in = data_to_send[7];
    fixture->clock_cycle();
    cr_assert(fixture->verilated_module->data_valid);
    cr_assert_eq(fixture->verilated_module->space_left, 0);

    fixture->verilated_module->write_en = 0;
    fixture->verilated_module->read_en = 1;
    fixture->clock_cycle();
    cr_assert(fixture->verilated_module->data_valid);
    cr_assert_eq(fixture->verilated_module->data_out, data_to_send[0]);
    cr_assert_eq(fixture->verilated_module->space_left, 1);

    fixture->verilated_module->read_en = 0;
    fixture->verilated_module->write_en = 1;
    fixture->verilated_module->data_in = data_to_send[8];
    fixture->clock_cycle();
    cr_assert(fixture->verilated_module->data_valid);
    cr_assert_eq(fixture->verilated_module->space_left, 0);

    for (int i = 1; i < 7; i++) {
        fixture->verilated_module->write_en = 0;
        fixture->verilated_module->read_en = 1;
        fixture->clock_cycle();
        cr_assert(fixture->verilated_module->data_valid);
        cr_assert_eq(fixture->verilated_module->data_out, data_to_send[i]);
    }

    fixture->verilated_module->write_en = 0;
    fixture->verilated_module->read_en = 1;
    fixture->clock_cycle();
    cr_assert(fixture->verilated_module->data_valid);
    cr_assert_eq(fixture->verilated_module->data_out, data_to_send[8]);
    cr_assert_eq(fixture->verilated_module->space_left, fixture->length);
}
