// v1
//  ONLY standard mode
//  first for single read/write

// follows wb4 spec
module formal_wbslave
    #(
        ADDR_WIDTH          = 4,
        DATA_WIDTH          = 64,
        SEL_WIDTH           = 8
    )
    (
        // see rule 3.40 for which signals have to be present
        input   wire                        clk,
        input   wire                        rst_i,
        input   wire[ADDR_WIDTH-1:0]        adr_i,
        input   wire[DATA_WIDTH-1:0]        dat_i,
        input   wire[SEL_WIDTH-1:0]         sel_i,
        input   wire                        we_i,
        input   wire                        stb_i,
        input   wire                        cyc_i,

        input   wire[DATA_WIDTH-1:0]        dat_o,
        input   wire                        ack_o,
        input   wire                        err_o
    );

    // some covers:
    //  things that must be encountered at least once
    always @(*)
    begin
        cover (ack_o);
        cover (err_o);
        cover (rst_i);
        cover (stb_i & cyc_i);
    end

    // register indicating if $past() can be used
    //  (inspired by formal verification articles on zipcpu blog)
    reg     past_valid;
    initial past_valid = 1'b0;
    always @(posedge clk) past_valid <= 1'b1;

    // assume reset in the beginning if past is not valid
    always @(*) if (!past_valid) assume(rst_i);
    initial assume(rst_i);

    initial assume (!cyc_i);
    initial assume (!stb_i);
    initial assert (!ack_o);
    initial assert (!err_o);
    always @(posedge clk)
    begin
        if ((!past_valid) || $past(rst_i)) // rule 3.00
        begin
            assume (!cyc_i); // rule 3.20
            assume (!stb_i); // rule 3.20
            assert (!ack_o); // rule 3.35
            assert (!err_o); // rule 3.35
        end
    end

    always @(*)
    begin
        if (stb_i)
            assume(cyc_i); // rule 3.25
    end

    always @(posedge clk)
        if (ack_o || err_o)
            assert (stb_i && cyc_i); // rule 3.35
    always @(*) assert ((ack_o + err_o) < 2); // rule 3.45

    // rule 3.50
    always @(posedge clk)
    begin
        if (past_valid)
        begin
            if ($past(stb_i) & $past(ack_o || err_o) & (!stb_i))
                assert(!(ack_o || err_o));
        end
    end

    // rule 3.25 (sort of)
    always @(posedge clk)
    begin
        if (past_valid && $past(ack_o || err_o))
        begin
            assume (!stb_i);
            assume (!cyc_i);
        end
    end

    // rule 3.75: classic standard single read/write timing
    reg pending_request;
    initial pending_request = 1'b0;
    always @(posedge clk)
    begin
        if (rst_i)
            pending_request <= 1'b0;
        else
        begin
            if (stb_i & cyc_i)
            begin
                if (!pending_request)
                    assert (!$past (ack_o || err_o));
                pending_request <= 1'b1;
            end
            else
            begin
                if (pending_request)
                    assert ($past (ack_o || err_o));
                pending_request <= 1'b0;
            end
        end
    end
    always @(*)
        if (ack_o || err_o)
            assert (pending_request);
    always @(*)
        if (pending_request)
        begin
            // wait state -> master must keep stb_i and cyc_i high
            assume (stb_i);
            assume (cyc_i);
        end

endmodule
