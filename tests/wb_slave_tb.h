#ifndef WB_SLAVE_TB
#define WB_SLAVE_TB

#include "test_bench.h"
#include "Vwb_slave.h"

#include <criterion/criterion.h>

class WbSlaveTb : public ClockedTestBench<Vwb_slave> {
    public:
        WbSlaveTb (int _clock_period)
            : ClockedTestBench<Vwb_slave> (_clock_period) {};
        WbSlaveTb (bool trace, std::string trace_file_name, int _clock_period)
            : ClockedTestBench<Vwb_slave> (trace, trace_file_name, _clock_period) {};
        virtual ~WbSlaveTb () {};
};

static WbSlaveTb *fixture;

#endif
