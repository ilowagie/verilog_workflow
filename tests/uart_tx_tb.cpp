#include "uart_tx_tb.h"

#define PRINT false

static void tb_fixture_setup (void) {
    fixture = new UartTxTb(10, 4);
}

static void tb_fixture_tear_down (void) {
    delete fixture;
}

Test(uart_tx, send_1_symbol,
     .init = tb_fixture_setup,
     .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/uart_tx/send_1_symbol.vcd");

    // setting input data to "0010 1000"  which is the character 'H' in ASCII
    uint8_t data_to_send = 0x48;

    fixture->verilated_module->rst = 0;
    fixture->clock_cycle();

    // since we've used initial statements, we can just start working without doing a reset
    cr_assert_eq(fixture->verilated_module->busy, 0);
    cr_assert_eq(fixture->verilated_module->serial_uart_out, 1);
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->busy, 0);
    cr_assert_eq(fixture->verilated_module->serial_uart_out, 1);

    fixture->verilated_module->parallel_data_in = data_to_send;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->busy, 0); // no req sent -> should not start
    cr_assert_eq(fixture->verilated_module->serial_uart_out, 1);

    fixture->verilated_module->req = 1;
    fixture->clock_cycle();
    fixture->verilated_module->req = 0;
    cr_assert_eq(fixture->verilated_module->busy, 1); // req sent -> should be busy
    cr_assert_eq(fixture->verilated_module->serial_uart_out, 0); // start bit
    fixture->clock_cycle();
    // and check if correct bits are sent
    for (int step_n = 0; step_n < 8; step_n++) {
        auto expected = ((data_to_send & (0x80 >> step_n)) != 0);
#if PRINT
        std::cerr << "expected: " << expected << std::endl;
        fprintf(stderr, "got: %d\n", fixture->verilated_module->serial_uart_out);
#endif
        cr_assert_eq(fixture->verilated_module->serial_uart_out, expected);
        fixture->clock_cycle();
    }
    cr_assert_eq(fixture->verilated_module->serial_uart_out, 1); // stop bit

    fixture->clock_cycle();
    // end with a check whether busy has returned to 0
    cr_assert_eq(fixture->verilated_module->busy, 0); // no req sent -> should not start
}
