#ifndef UART_BUFFER_TB
#define UART_BUFFER_TB

#include "test_bench.h"
#include "Vuart_buffer.h"

#include <criterion/criterion.h>

class UartBufferTb : public ClockedTestBench<Vuart_buffer> {
    public:
        int width;
        int length;

        UartBufferTb (int _clock_period)
            : ClockedTestBench<Vuart_buffer> (_clock_period),
            width(8), length(7) {};
        UartBufferTb (bool trace, std::string trace_file_name, int _clock_period)
            : ClockedTestBench<Vuart_buffer> (trace, trace_file_name, _clock_period),
            width(8), length(7) {};
        virtual ~UartBufferTb () {};
};

static UartBufferTb *fixture;

#endif
