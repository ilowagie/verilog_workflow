#include "wb_slave_tb.h"

static void tb_fixture_setup (void) {
    fixture = new WbSlaveTb (10);
    // making sure input signals are set
    // these 2 are important
    fixture->verilated_module->cyc_i = 0;
    fixture->verilated_module->stb_i = 0;
    // these are 'don't care' at the start
    fixture->verilated_module->we_i  = 0;
    // setting sel_i to 1 avoids forgetting it
    // since we don't really use it because the data width is 8
    fixture->verilated_module->sel_i = 1;
    fixture->verilated_module->adr_i = 0;
    fixture->verilated_module->dat_i = 0;
    // these would come from inside
    fixture->verilated_module->tx_buf_space = 4; // somthing random that isn't 0
    fixture->verilated_module->rx_valid     = 0;
    fixture->verilated_module->rx_data      = 0;
}

static void tb_fixture_tear_down (void) {
    delete fixture;
}

Test(wb_slave, write_no_wait,
        .init = tb_fixture_setup,
        .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/wb_slave_tb/write_no_wait.vcd");

    uint8_t data_to_write = 0x48;

    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);

    // trying to trick module to ack prematurely
    fixture->verilated_module->dat_i = data_to_write;
    fixture->verilated_module->we_i = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);

    // ok, now we really request the write
    // module should handle it immediately bacause of setup
    fixture->verilated_module->cyc_i = 1;
    fixture->verilated_module->stb_i = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 1);
    cr_assert_eq(fixture->verilated_module->tx_req, 1);
    cr_assert_eq(fixture->verilated_module->tx_data, data_to_write);
    fixture->verilated_module->cyc_i = 0;
    fixture->verilated_module->stb_i = 0;

    // ack should return to 0 after cycle
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);
}

Test(wb_slave, write_wait,
        .init = tb_fixture_setup,
        .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/wb_slave_tb/write_wait.vcd");

    uint8_t data_to_write = 0x48;
    fixture->verilated_module->tx_buf_space = 0;

    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);

    // request write, but with tx_buf_space = 0
    // so request cannot be handled now -> no ack
    fixture->verilated_module->dat_i = data_to_write;
    fixture->verilated_module->we_i = 1;
    fixture->verilated_module->cyc_i = 1;
    fixture->verilated_module->stb_i = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);

    // make a tx_buf_space available
    // module should handle and ack write request now
    fixture->verilated_module->tx_buf_space = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 1);
    cr_assert_eq(fixture->verilated_module->tx_req, 1);
    cr_assert_eq(fixture->verilated_module->tx_data, data_to_write);
    fixture->verilated_module->cyc_i = 0;
    fixture->verilated_module->stb_i = 0;

    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);
}

Test(wb_slave, read_tx_space,
        .init = tb_fixture_setup,
        .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/wb_slave_tb/read_tx_space.vcd");

    // tx starts with a space of 4 -> see setup
    // first request should return 4

    fixture->clock_cycle();
    fixture->verilated_module->adr_i = 1;
    fixture->verilated_module->cyc_i = 1;
    fixture->verilated_module->stb_i = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 1);
    cr_assert_eq(fixture->verilated_module->dat_o, 4);
    fixture->verilated_module->cyc_i = 0;
    fixture->verilated_module->stb_i = 0;

    // now make tx_buf_space 0 and read that
    fixture->verilated_module->tx_buf_space = 0;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);
    fixture->verilated_module->cyc_i = 1;
    fixture->verilated_module->stb_i = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 1);
    cr_assert_eq(fixture->verilated_module->dat_o, 0);
    fixture->verilated_module->cyc_i = 0;
    fixture->verilated_module->stb_i = 0;

    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);
}

Test(wb_slave, read_data,
        .init = tb_fixture_setup,
        .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/wb_slave_tb/read_data.vcd");

    uint8_t data_to_read = 0x63; // something random
    fixture->verilated_module->rx_data = data_to_read;

    fixture->clock_cycle();
    fixture->verilated_module->adr_i = 3;
    fixture->verilated_module->cyc_i = 1;
    fixture->verilated_module->stb_i = 1;
    fixture->clock_cycle();
    // not yet valid
    cr_assert_eq(fixture->verilated_module->ack_o, 0);

    fixture->verilated_module->rx_valid = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 1);
    cr_assert_eq(fixture->verilated_module->dat_o, data_to_read);

    fixture->verilated_module->cyc_i = 0;
    fixture->verilated_module->stb_i = 0;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);
}

Test(wb_slave, read_rx_valid,
        .init = tb_fixture_setup,
        .fini = tb_fixture_tear_down) {

    fixture->set_trace_file("tests/wb_slave_tb/read_rx_valid.vcd");

    // lets start out with valid data
    fixture->verilated_module->rx_valid = 1;
    fixture->verilated_module->rx_data = 0x55;
    fixture->clock_cycle();
    fixture->verilated_module->adr_i = 2;
    fixture->verilated_module->cyc_i = 1;
    fixture->verilated_module->stb_i = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 1);
    cr_assert_eq(fixture->verilated_module->dat_o, 1);
    fixture->verilated_module->cyc_i = 0;
    fixture->verilated_module->stb_i = 0;

    fixture->verilated_module->rx_valid = 0;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);
    fixture->verilated_module->cyc_i = 1;
    fixture->verilated_module->stb_i = 1;
    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 1);
    cr_assert_eq(fixture->verilated_module->dat_o, 0);

    fixture->verilated_module->cyc_i = 0;
    fixture->verilated_module->stb_i = 0;

    fixture->clock_cycle();
    cr_assert_eq(fixture->verilated_module->ack_o, 0);
}
