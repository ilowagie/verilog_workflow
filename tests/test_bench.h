#ifndef TESTBENCH_H
#define TESTBENCH_H

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <verilated.h>
#include <verilated_vcd_c.h>

template <class VM>
class TestBench {
    protected:
        bool            trace;
        VerilatedVcdC   *trace_file;
        vluint64_t      main_time;
    public:
        VM              *verilated_module;

        TestBench() {
            this->verilated_module = new VM;
            this->trace = false;
            this->trace_file = NULL;
            this->main_time = 0;
        }
        TestBench(bool trace, const std::string trace_file_name) {
            this->verilated_module = new VM;
            this->trace = trace;
            this->main_time = 0;

            if (trace)
            {
                Verilated::traceEverOn(true);
                this->trace_file = new VerilatedVcdC;
                this->verilated_module->trace(this->trace_file, 99); // trace ALL signals
                Verilated::mkdir("logs");
                this->trace_file->open(("logs/" + trace_file_name).c_str());
            }
            else
                this->trace_file = NULL;
        }

        virtual ~TestBench() {
            this->verilated_module->final();
            if (this->trace)
                this->trace_file->close();
            delete this->verilated_module;
        }

        virtual void set_trace_file(const std::string file_path) {
            if (!this->trace) {
                this->trace = true;
                Verilated::traceEverOn(true);
                this->trace_file = new VerilatedVcdC;
                this->verilated_module->trace(this->trace_file, 99); // trace ALL signals
                Verilated::mkdir("logs");
            } else {
                // this->trace is true, so trace_file is an opened file
                // we need to close it before opening a new one
                this->trace_file->close();
            }

            // find out all the subdirectories in the file path and make them
            std::string built_path = "logs/";
            std::stringstream file_path_stream (file_path);
            std::vector<std::string> path_parts;
            std::string tmp_string;
            // split the string into parts delimited with a '/'
            while (std::getline(file_path_stream, tmp_string, '/')) {
                path_parts.push_back(tmp_string);
            }
            // take out the last part of the vector i.e. the filename
            // and save for later
            std::string filename = path_parts.back();
            path_parts.pop_back();
            // make the directories that we found
            for (std::string part : path_parts) {
                built_path += part + '/';
                Verilated::mkdir(built_path.c_str());
            }
            built_path += filename;
            this->trace_file->open(built_path.c_str());
        }

        virtual void tick() {
            this->main_time++;
            this->verilated_module->eval();
            if (this->trace)
                this->trace_file->dump(this->main_time);
        }


        const void run_for(vluint64_t time_to_run) {
            vluint64_t start_time = this->main_time;
            while (!Verilated::gotFinish() && ((this->main_time - start_time) < time_to_run))
                this->tick();
        }
};

template <class VM>
class ClockedTestBench : public TestBench<VM> {
    protected:
        int     clock_period;
    public:
        ClockedTestBench (int _clock_period)
            : TestBench<VM> (), clock_period(_clock_period) {};
        ClockedTestBench (bool trace, std::string trace_file_name, int _clock_period)
            : TestBench<VM> (trace, trace_file_name), clock_period(_clock_period) {};
        virtual ~ClockedTestBench () {};

        virtual void toggle_clock() {
            this->verilated_module->clk = (this->verilated_module->clk) ? 0 : 1;
            this->verilated_module->eval();
        };

        virtual void clock_cycle() {
            this->main_time += this->clock_period;

            this->verilated_module->clk = 0;
            this->verilated_module->eval();

            if (this->trace) {
                // there is a lot more going on when using traces, because we want things to look beautiful.
                // If we care about traces, we need to let outputs settle a bit.
                // otherwise, it might look like they get triggered on the falling edge.
                // this is be very confusing.

                int delta = this->clock_period / 5;
                int half_period = this->clock_period / 2;

                this->trace_file->dump(this->main_time - delta); // dump right before cycle
                this->toggle_clock();
                this->trace_file->dump(this->main_time);
                this->toggle_clock();
                this->trace_file->dump(this->main_time + half_period);

                this->trace_file->flush(); // flush trace_file I/O

            } else {
                // if we don't trace, all that extra logic is unneeded,
                // just make sure the output signals are correct

                this->toggle_clock(); // up
                this->toggle_clock(); // down

            }
        };

        const void n_clock_cycles(int n) {
            for (int i = 0; i < n; i++)
                this->clock_cycle();
        };
};

#endif
