#ifndef UART_TX_TB
#define UART_TX_TB

#include "test_bench.h"
#include "Vuart_tx.h"

#include <criterion/criterion.h>

class UartTxTb : public ClockedTestBench<Vuart_tx> {
    private:
        int     clock_division;
    public:
        UartTxTb (int _clock_period, int _clock_division)
            : ClockedTestBench<Vuart_tx> (_clock_period), clock_division(_clock_division) {};
        UartTxTb (bool trace, std::string trace_file_name, int _clock_period, int _clock_division)
            : ClockedTestBench<Vuart_tx> (trace, trace_file_name, _clock_period), clock_division(_clock_division) {};
        virtual ~UartTxTb () {};

        virtual void clock_cycle() {
            for (int i = 0; i < this->clock_division; i++) {
                ClockedTestBench<Vuart_tx>::clock_cycle();
            }
        };
};

static UartTxTb *fixture;

#endif
